"""
1- kullanicidan imput alincak (baslangic, bitis, dosya_path)
2 - dosya olusturulmasi gerekiyor
3- ciktilarin dosya yazilmasi

"""

# take input values

n_baslangic = int(input("Enter beginning value: "))
n_bitis = int(input("Enter finish value: "))
s_file_path = input("Enter file path: ")
print(n_baslangic, "\n", n_bitis, "\n", s_file_path)

# create folder
f_range_file = open(s_file_path, "w")

# write folder
# begin and finish values are numbers but write() expect input as string
# thats why you need to convert int values into string
"""
for usage:
for index in (where for wander around) place 
"""
for index in range(n_baslangic, n_bitis):
    print(type(index)) # this is for variable type checking
    f_range_file.write(str(index))
    # seperator is inserted
    f_range_file.write("*")
f_range_file.write(str(n_bitis))