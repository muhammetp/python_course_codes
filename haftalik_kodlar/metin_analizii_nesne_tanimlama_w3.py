import docx

'''
in text analysis steps to analyze are as follows

1- obtain the text data from file
2- store all text as  string on code
3- clean up the text with operations such remove all comma, punctuations,conjuctions
which are repeated for many times and not informative
4- count up remaining words using dictionary data sturcture to compose the histogram word counts
 '''


'''obtain the text data from file '''
file_name = "../data/news.docx"


def get_text(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


def replace_all_redundant_chars(inp_full_text, inp_char_list, inp_b_w_space):
    for char in inp_char_list:
        if inp_b_w_space:
            inp_full_text = inp_full_text.replace(char, " ")
        else:
            inp_full_text = inp_full_text.replace(char, "")
    return inp_full_text


def count_words(inp_full_text):
    dict_word_counter = {}
    '''save full text  as word by word '''
    word_text = inp_full_text.split(" ")
    for word in word_text:
        if word in dict_word_counter:
            dict_word_counter[word] += 1
        else:
            dict_word_counter[word] = 1

    return {k: v for k, v in sorted(dict_word_counter.items(), key=lambda item: item[1])}

'''store all text as  string on code'''
full_text = get_text(file_name)
print(full_text)

print('''\nclean up the text with operations such remove all comma, punctuations,conjuctions
which are repeated for many times and not informative\n''')

char_list_wo_space = [",", "yor.", "."]
char_list_w_space = [" ve ", " için ", " de ", "yor ", "da ", " the ", " to ", " and ", " a ", " of ", " that "
                     ' are ', ' be ', ' is ', ' for ', ' said ', ' on ', ' in ', ' with ', ' he ', ' by ']
b_change_w_space = True
full_text = replace_all_redundant_chars(full_text, char_list_w_space, b_change_w_space)
b_change_w_space = False
full_text = replace_all_redundant_chars(full_text, char_list_wo_space, b_change_w_space)
print(full_text)
print(count_words(full_text))


