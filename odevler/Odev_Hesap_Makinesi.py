import time
print("""Hesap Makinesi Uygulaması
-----------------------------
Yapılabilecek İşlemler
----------------------
Toplama işlemi yapmak için : Toplama veya +
Çıkarma işlemi yapmak için : Çıkarma veya -
Çarpma işlemi yapmak için : Çarpma veya *
Bölme işlemi yapmak için : Bölme veya /
Hesap makinesi uygulamasından çıkmak için 'q' yazınız""")

islem_sembol_dönüstürücü = {"+":"Toplama","-":"Çıkarma","*":"Çarpma","/":"Bölme"}

while(True):

    islem_sec = input("Yapmak istediğiniz işlemi seçiniz : ")

    if(islem_sec == 'Toplama' or islem_sembol_dönüstürücü[islem_sec] == "Toplama"):

        sayi1 = int(input("İşlem yapmak istediğiniz 1.sayıyı giriniz :"))
        sayi2 = int(input("İşlem yapmak istediğiniz 2.sayıyı giriniz :"))

        sonuc = sayi1 + sayi2
        print("{} + {} : {}".format(sayi1,sayi2,sonuc))

    elif(islem_sec == 'Çıkarma'or islem_sembol_dönüstürücü[islem_sec] == "Çıkarma"):

        sayi1 = int(input("İşlem yapmak istediğiniz 1.sayıyı giriniz :"))
        sayi2 = int(input("İşlem yapmak istediğiniz 2.sayıyı giriniz :"))

        sonuc = sayi1 - sayi2
        print("{} - {} : {}".format(sayi1, sayi2, sonuc))

    elif(islem_sec == 'Çarpma'or islem_sembol_dönüstürücü[islem_sec] == "Çarpma"):

        sayi1 = int(input("İşlem yapmak istediğiniz 1.sayıyı giriniz :"))
        sayi2 = int(input("İşlem yapmak istediğiniz 2.sayıyı giriniz :"))

        sonuc = sayi1 * sayi2
        print("{} * {} : {}".format(sayi1, sayi2, sonuc))

    elif(islem_sec == 'Bölme'or islem_sembol_dönüstürücü[islem_sec] == "Bölme"):

        sayi1 = int(input("İşlem yapmak istediğiniz 1.sayıyı giriniz :"))
        sayi2 = int(input("İşlem yapmak istediğiniz 2.sayıyı giriniz :"))
        if sayi2 == 0:
            """Bu seçenek ifadeyi başa döndürür"""
            continue
            """while (sayi2 == 0):
                # Bu seçenek doğru sayıyı alıncaya kadar ısrar eder
                print("Lütfen 0'dan farklı bir sayı giriniz.")
                sayi2 = int(input("İşlem yapmak istediğiniz 2.sayıyı giriniz :"))"""

        sonuc = sayi1 / sayi2
        print("{} / {} : {}".format(sayi1, sayi2, sonuc))

    elif(islem_sec == 'q'):

        print("PROGRAM SONLANDIRILIYOR...")
        time.sleep(1)
        print("Program Sonlandırıldı.")
        break